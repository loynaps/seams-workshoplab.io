---
layout: page
title: Workshop
permalink: /workshop/
---
Our *SEAMS Workshop* extends the [Principles](/principles/) course to include a hack-a-thon during the second week.  The topic focus and schedule for the first week remains largely the same, though we integrate development of a work plan for the hack-a-thon.

During the hack-a-thon week, the schedule is more flexible. Participants meet with faculty on a regularly scheduled basis to check-in on how their plan is progressing, but *they set that plan*. The workshop wraps with participants demonstrating changes to their project's engineering.
